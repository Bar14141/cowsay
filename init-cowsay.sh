#!/bin/bash
docker build -t cowsay .
if [ $# -eq 0 ]
then
echo -e '\n\n\nusage: ./init-cowsay.sh [external-port] [internal-port]\n\n'
echo -e '\n\nNo ports entered executing server on default port 8080\n\n'
docker run -d -p 8080:8080 cowsay
echo avliable at http://localhost:/8080
else
docker run -d -p $1:$2 cowsay $2
link=http://localhost:/$1/
echo -e '\n\navliable at '$link'\n\n'
fi
 
 
 
 
